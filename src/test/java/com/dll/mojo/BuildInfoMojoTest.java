package com.dll.mojo;

import org.apache.maven.plugin.testing.MojoRule;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.configuration.PlexusConfiguration;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BuildInfoMojoTest {

    @Rule
    public MojoRule rule = new MojoRule() {
        @Override
        protected void before() throws Throwable {
        }

        @Override
        protected void after() {
        }
    };

    @Test
    public void testGitProject() throws Exception {
        File pom = new File("src/test/resources/gitproject/pom.xml");
        PlexusConfiguration config = rule.extractPluginConfiguration("build-info-maven-plugin", pom);
        MavenProject project = rule.readMavenProject(pom.getParentFile());
        BuildInfoMojo mojo = (BuildInfoMojo) rule.lookupConfiguredMojo(project, "build-info");
        mojo = (BuildInfoMojo) rule.configureMojo(mojo, config);

        mojo.execute();

        Properties props = mojo.projectProperties();

        assertEquals(props.getProperty("info.git.branch"), "master");
        assertNotNull(props.getProperty("info.api.docs"));
    }

    @Test
    public void testEmptyProject() throws Exception {
        File pom = new File("src/test/resources/emptyproject/pom.xml");
        PlexusConfiguration config = rule.extractPluginConfiguration("build-info-maven-plugin", pom);
        MavenProject project = rule.readMavenProject(pom.getParentFile());
        BuildInfoMojo mojo = (BuildInfoMojo) rule.lookupConfiguredMojo(project, "build-info");
        mojo = (BuildInfoMojo) rule.configureMojo(mojo, config);

        mojo.execute();

        Properties props = mojo.projectProperties();

        assertEquals(null, props.getProperty("info.git.branch"));
    }

}