package com.dll.mojo;
/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.util.Properties;

public abstract class AbstractDefinePropertyMojo extends AbstractMojo {
    /**
     * The maven project
     */
    @Parameter(readonly = true, defaultValue = "${project}")
    private MavenProject project;

    protected void setProperty(String name, String value) {
        if (getLog().isDebugEnabled()) {
            getLog().debug( "defined property " + name + " = \"" + value + "\"" );
        }

        project.getProperties().put( name, value );
    }

    public Properties projectProperties() {
        return getProject().getProperties();
    }

    /**
     * Get the current project instance.
     *
     * @return the project
     */
    public MavenProject getProject() {
        return project;
    }
}