package com.dll.mojo;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.model.*;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Config;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import static java.lang.String.format;
import static java.lang.System.getenv;

@Mojo(name = "build-info", defaultPhase = LifecyclePhase.VALIDATE, threadSafe = true)
public class BuildInfoMojo extends AbstractDefinePropertyMojo {

    enum POM_PROPS {
        URL("url"),
        BUILD_NUMBER("build.number"),
        DEVELOPER("developer"),
        CONTRIBUTOR("contributor"),
        MAILING_LIST("mailing.list"),
        TASK_MGMT("task.mgmt"),
        CI_MGMT("ci.mgmt");

        String value;
        POM_PROPS(String value) {
            this.value = value;
        }
        public String toString() {
            return value;
        }
    }

    enum GIT_PROPS {
        BRANCH("git.branch"),
        COMMIT("git.commit"),
        REMOTE("git.remote");

        String value;
        GIT_PROPS(String value) {
            this.value = value;
        }
        public String toString() {
            return value;
        }
    }

    /**
     * Prefix to append to the generated properties
     */
    @Parameter(defaultValue = "info", required = true)
    private String prefix;

    /**
     * Extra properties to add to the project
     */
    @Parameter
    private Map<Object, Object> extra;

    /**
     * Forces project to contain url, ci, issue, developer, etc. (contributors are optional)
     */
    @Parameter(defaultValue = "true")
    private boolean enforcePomInfo;

    /**
     * Enable to fetch information about the project's Git repository (revisions, branch, etc.)
     */
    @Parameter(defaultValue = "true")
    private boolean git;

    /**
     * How many git commits to fetch.
     */
    @Parameter(defaultValue = "5")
    private int gitMaxCommits;

    /**
     *
     */
    @Parameter(defaultValue = "${project.basedir}")
    private File gitRepositoryPath;

    /**
     * Enable to print the project properties
     */
    @Parameter(defaultValue = "false")
    private boolean debug;

    private Properties mergeProperties(Properties props) {

        for (Entry entry : props.entrySet()) {
            projectProperties().put(prefix(entry.getKey().toString()), entry.getValue().toString());
        }

        return projectProperties();
    }

    private String prefix(String value) {
        return prefix + '.' + value;
    }

    Properties gitInformation(Properties props) throws Exception {
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        Repository repo = builder
                .findGitDir(gitRepositoryPath) // scan up the file system tree
                .readEnvironment() // scan environment GIT_* variables
                .build();
        Config config = repo.getConfig();
        Git git = new Git(repo);

        props.put(GIT_PROPS.BRANCH, firstNonNull(repo.getBranch(), ""));

        int commitCounter = 0;
        for (RevCommit commit : git.log().call()) {
            PersonIdent author = commit.getAuthorIdent();
            props.put(GIT_PROPS.COMMIT + "." + commitCounter,
                    format("%s %s %s",
                            commit.name(), author.toString().replace("PersonIdent", ""),
                            commit.getShortMessage()));

            commitCounter++;
            if (commitCounter == gitMaxCommits)
                break;
        }

        String originRemote = config.getString("remote", "origin", "url");
        props.put(GIT_PROPS.REMOTE, firstNonNull(originRemote, ""));

        return props;
    }

    Properties extraInformation(Properties props) {
        for (Entry it : extra.entrySet()) {
            props.put(it.getKey(), firstNonNull(it.getValue(), ""));
        }
        return props;
    }

    Properties pomInformation(Properties props) {
        Model model = getProject().getModel();

        String url = model.getUrl();
        if (url != null) {
            props.put(POM_PROPS.URL, url);
        } else if (enforcePomInfo) {
            throw new RuntimeException(format("Mandatory POM field=%s", "url"));
        }

        String buildNumberProp = model.getProperties().getProperty(POM_PROPS.BUILD_NUMBER.toString());
        String buildNumber = firstNonNull(buildNumberProp, getenv("BUILD_NUMBER"), null);
        if (buildNumber != null) {
            props.put(POM_PROPS.BUILD_NUMBER, buildNumber);
        } else if (enforcePomInfo) {
            throw new RuntimeException(format("Mandatory POM property=%s", "build.number"));
        }

        int counter = 0;
        for (Developer it : model.getDevelopers()) {
            props.put(POM_PROPS.DEVELOPER + "." + counter, format("%s<%s>", it.getName(), it.getEmail()));
            counter++;
        }
        if (enforcePomInfo && counter == 0) {
            throw new RuntimeException(format("Mandatory POM field=%s", "developers"));
        }

        counter = 0;
        for (Contributor it : model.getContributors()) {
            props.put(POM_PROPS.CONTRIBUTOR + "." + counter, format("%s<%s>", it.getName(), it.getEmail()));
            counter++;
        }
        if (enforcePomInfo && counter == 0) {
            //throw new RuntimeException(format("Mandatory POM field=%s", "contributors"));
            getLog().info(format("Optional POM field=%s is empty", "contributors"));
        }

        counter = 0;
        for (MailingList it : model.getMailingLists()) {
            props.put(POM_PROPS.MAILING_LIST + "." + counter,
                    format("%s[%s]", it.getName(),
                            firstNonNull(it.getArchive(), it.getPost(), it.getSubscribe(), it.getUnsubscribe())));
            counter++;
        }
        if (enforcePomInfo && counter == 0) {
            throw new RuntimeException(format("Mandatory POM field=%s", "mailingLists"));
        }

        IssueManagement issueMgmt = model.getIssueManagement();
        if (issueMgmt != null) {
            props.put(POM_PROPS.TASK_MGMT, format("%s[%s]", issueMgmt.getSystem(), issueMgmt.getUrl()));
        } else if(enforcePomInfo) {
            throw new RuntimeException(format("Mandatory POM field=%s", "issueManagement"));
        }

        CiManagement ciMgmt = model.getCiManagement();
        if (ciMgmt != null) {
            props.put(POM_PROPS.CI_MGMT, format("%s[%s]", ciMgmt.getSystem(), ciMgmt.getUrl()));
        } else if(enforcePomInfo) {
            throw new RuntimeException(format("Mandatory POM field=%s", "ciManagement"));
        }

        return props;
    }

    public <T> T firstNonNull(T... objs) {
        for (T obj : objs) {
            if (obj != null) {
                return obj;
            }
        }
        return null;
    }

    public void execute() throws MojoExecutionException, MojoFailureException {
        Properties props = new Properties();
        try {
            if (debug) {
                printProps();
            }

            if (git) {
                props = gitInformation(props);
            }

            if (extra != null) {
                props = extraInformation(props);
            }

            props = pomInformation(props);

            mergeProperties(props);

            if (debug) {
                getLog().info("New Build Info properties: " + projectProperties());
            }

        } catch (Exception e) {
            throw new MojoExecutionException("Failed to generate build info", e);
        }
    }

    private void printProps() {
        getLog().info("Available properties:");
        for (POM_PROPS prop : POM_PROPS.values()) {
            getLog().info(prefix(prop.toString()));
        }
        for (GIT_PROPS prop : GIT_PROPS.values()) {
            getLog().info(prefix(prop.toString()));
        }
    }

}
