# Build Info Maven Plugin

Forces project POM to contain information on url, ci, issue, developer, etc. (contributors are optional).
Extracts the later and GIT repo information into the POM object to be used with resource filters.